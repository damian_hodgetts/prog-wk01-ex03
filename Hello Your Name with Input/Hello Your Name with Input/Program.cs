﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Your_Name_with_Input
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "";
            var myAge = 0;
            var brotherage = 3;


            Console.WriteLine("Please enter your name:");
            myName = Console.ReadLine();

            Console.WriteLine("Please enter your age:");
            myAge = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hi, my name is {myName} my brothers age is {brotherage} and my age is {myAge - brotherage}");

        }
    }
}
